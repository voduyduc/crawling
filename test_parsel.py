from parsel import Selector
from insert_parsel import insert_news
import urllib.request
import datetime

global url
url = 'https://news.ycombinator.com'

class NewsPasre:        
    def parse(self, url):
        
        x = urllib.request.urlopen(url).read()
        x = x.decode("utf-8")
        response = Selector(x)

        for news in response.css('.athing'):
                
            id = news.css('::attr(id)').get()
            rank = news.css('.rank ::text').get()
            title = news.css('.title a ::text').get()
            link = news.css('.title a ::attr(href)').get()
            point = ""
            date = ""

            for n in response.css('.subtext'):
                score_id = n.css('.score ::attr(id)').get()
                age_id = n.css('.age a ::attr(href)').get()
                if (str(id) in str(score_id)) or (str(id) in str(age_id)):
                    point = n.css('.score ::text').get()
                    date = n.css('.age ::text').get()
                    break
            #insert_news(rank,title,link,point,date)
            d = ''.join(c for c in date if c.isdigit())
            d2 = datetime.datetime.now()
            
            d = int(d)
            
            if 'min' in date:
                d2 = d2 - datetime.timedelta(minutes = d)
            elif 'hour' in date:
                d2 = d2 - datetime.timedelta(hours = d)
            elif 'day' in date:
                d2 = d2 - datetime.timedelta(days = d)
            elif 'week' in date:
                d2 = d2 - datetime.timedelta(weeks = d)
            
            print(id)
            print(rank)
            print(title)
            print(point)
            print(date)
            print(d2)

        next_page = response.css('.morelink ::attr(href)').get()
        if next_page:
            url = 'https://news.ycombinator.com/'+next_page
            callback=self.parse(url)

a = NewsPasre()
a.parse(url)
