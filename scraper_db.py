import scrapy
import json
from insert import insert_news

class NewsSetSpider(scrapy.Spider):
    name = 'news'
    start_urls = ['https://news.ycombinator.com']
    
    def parse(self, response):
        

        SET_SELECTOR = '.athing'
        SET2_SELECTOR = '.subtext'

        for news in response.css(SET_SELECTOR):
                
            ID_SELECTOR = '::attr(id)'
            nid = news.css(ID_SELECTOR).extract_first()
            #print (nid)
            RANK_SELECTOR = '.rank ::text'
            TITLE_SELECTOR = '.title a ::text'
            LINK_SELECTOR = '.title a ::attr(href)'
            rank = news.css(RANK_SELECTOR).extract_first()
            title = news.css(TITLE_SELECTOR).extract_first()
            link = news.css(LINK_SELECTOR).extract_first()
            point = ""
            date = ""

            for n in response.css(SET2_SELECTOR):
                SCOREID_SELECTOR = '.score ::attr(id)'
                score_id = n.css(SCOREID_SELECTOR).extract_first()
                #print(score_id)
                if str(nid) in str(score_id):
                    POINT_SELECTOR = '.score ::text'
                    point = n.css(POINT_SELECTOR).extract_first()
                    DATE_SELECTOR = '.age ::text'
                    date = n.css(DATE_SELECTOR).extract_first()
                    #print(point)
                    #print(date)
                    break
            insert_news(rank,title,link,point,date)
            
        NEXT_PAGE_SELECTOR = '.morelink ::attr(href)'
        next_page = response.css(NEXT_PAGE_SELECTOR).extract_first()
        if next_page:
            yield scrapy.Request(
                response.urljoin('https://news.ycombinator.com/'+next_page),
                callback=self.parse
            )
